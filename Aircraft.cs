﻿using System;
using System.ComponentModel;

namespace AircraftConsole
{
    /// <summary>
    /// Class to hold state of Aircraft
    /// </summary>
    public class Aircraft
    {
        //public int ACID { get; set; }
        public string ACType { get; set; }
        public string ACSize { get; set; }
        public int Priority { get; set; }
        public DateTime CreatedDate { get; set; }

        /// <summary>
        /// Class Constructor. Priority: Large Passanger: 1 - Small Passanger: 2 - Large Cargo: 3 - Small Cargo: 4
        /// </summary>
        /// <param name="type">ACTypes</param>
        /// <param name="size">ACSizes</param>
        public Aircraft(ACTypes type, ACSizes size)
        {
            ACType = AircraftHelper.GetEnumDescription(type);
            ACSize = AircraftHelper.GetEnumDescription(size);
            CreatedDate = DateTime.Now;
            // set Priority based on AC type and AC size
            if (type == ACTypes.Passenger)
            {
                if (size == ACSizes.Large)
                {
                    Priority = 1;
                }
                else
                {
                    Priority = 2;
                }
            }
            else if (type == ACTypes.Cargo)
            {
                if (size == ACSizes.Large)
                {
                    Priority = 3;
                }
                else
                {
                    Priority = 4;
                }
            }
        }
    }

    /// <summary>
    /// Enum / Struct for Aircraft Types
    /// </summary>
    public enum ACTypes
    {
        [Description("Passenger")]
        Passenger,
        [Description("Cargo")]
        Cargo
    }

    /// <summary>
    /// Enum / Struct for Aircraft Size
    /// </summary>
    public enum ACSizes
    {
        [Description("Large")]
        Large,
        [Description("Small")]
        Small
    }

    /// <summary>
    /// Enum / Struct for Request Type
    /// </summary>
    public enum RequestTypes
    {
        Boot,
        Enqueue,
        Dequeue
    }
}
