# README #

This is air-traffic control system. To achieve this exercise, I decided to simulate Priority Queue (There are other ways to implement this like create a queue per priority). 

1. Obtain AC priority
1. Enqueue an AC based on Priority and if the same priority exists, then based on CreatedDate
1. For Dequeue, always dequeue the first item in the Queue since it is already maintained to keep the ordering based on Priority and Date

### Output ###

![output1.PNG](https://bitbucket.org/repo/9bzRqo/images/833145823-output1.PNG)

![output2.PNG](https://bitbucket.org/repo/9bzRqo/images/2525284543-output2.PNG)

![output3.PNG](https://bitbucket.org/repo/9bzRqo/images/2162099667-output3.PNG)