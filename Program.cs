﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace AircraftConsole
{
    class Program
    {
        /// <summary>
        /// Aircraft Queue - This is simulated as a Priority Queue
        /// </summary>
        public static List<Aircraft> ACQueue = new List<Aircraft>();

        static void Main(string[] args)
        {
            // Start the system
            aqm_request_process(RequestTypes.Boot);

            // Enqueue few ACs
            Console.WriteLine("*** CREATING SAMPLE QUEUE ***");
            CreateSampleEnqueues();
            Console.WriteLine("*** CREATE SAMPLE QUEUE COMPLETED ***");

            // show the list
            PrintACList(ACQueue);

            // Dequeue an AC
            aqm_request_process(RequestTypes.Dequeue);
            PrintACList(ACQueue);

            aqm_request_process(RequestTypes.Dequeue);
            PrintACList(ACQueue);

            aqm_request_process(RequestTypes.Dequeue);
            PrintACList(ACQueue);

            aqm_request_process(RequestTypes.Dequeue);
            PrintACList(ACQueue);

            aqm_request_process(RequestTypes.Dequeue);
            PrintACList(ACQueue);

            aqm_request_process(RequestTypes.Dequeue);
            PrintACList(ACQueue);

            aqm_request_process(RequestTypes.Dequeue);
            PrintACList(ACQueue);

            Console.ReadKey();
        }

        /// <summary>
        /// This method is responsible to create sample data
        /// </summary>
        private static void CreateSampleEnqueues()
        {
            Aircraft ac1 = new Aircraft(ACTypes.Cargo, ACSizes.Large);
            aqm_request_process(RequestTypes.Enqueue, ac1);
            PrintACList(ACQueue);
            System.Threading.Thread.Sleep(2000); // wait for 2 second

            Aircraft ac2 = new Aircraft(ACTypes.Passenger, ACSizes.Small);
            aqm_request_process(RequestTypes.Enqueue, ac2);
            PrintACList(ACQueue);
            System.Threading.Thread.Sleep(2000);

            Aircraft ac3 = new Aircraft(ACTypes.Passenger, ACSizes.Large);
            aqm_request_process(RequestTypes.Enqueue, ac3);
            PrintACList(ACQueue);
            System.Threading.Thread.Sleep(2000);

            Aircraft ac4 = new Aircraft(ACTypes.Cargo, ACSizes.Small);
            aqm_request_process(RequestTypes.Enqueue, ac4);
            PrintACList(ACQueue);
            System.Threading.Thread.Sleep(2000);

            Aircraft ac5 = new Aircraft(ACTypes.Passenger, ACSizes.Small);
            aqm_request_process(RequestTypes.Enqueue, ac5);
            PrintACList(ACQueue);
            System.Threading.Thread.Sleep(2000);

            Aircraft ac6 = new Aircraft(ACTypes.Passenger, ACSizes.Large);
            aqm_request_process(RequestTypes.Enqueue, ac6);
            PrintACList(ACQueue);
            System.Threading.Thread.Sleep(2000);

            Aircraft ac7 = new Aircraft(ACTypes.Cargo, ACSizes.Large);
            aqm_request_process(RequestTypes.Enqueue, ac7);
            PrintACList(ACQueue);
        }

        /// <summary>
        /// request process method. 
        /// </summary>
        /// <param name="request">RequestTypes</param>
        /// <param name="ac">Aircraft</param>
        static void aqm_request_process(RequestTypes request, Aircraft ac = null)
        {
            string status = string.Empty;
            if(request == RequestTypes.Boot)
            {
                status = Boot();
            }
            switch (request)
            {
                case RequestTypes.Boot:
                    status = Boot();
                    break;
                case RequestTypes.Enqueue:
                    status = Enqueue(ac);
                    break;
                case RequestTypes.Dequeue:
                    status = Dequeue();
                    break;
                default:
                    status = "Invalid request!";
                    break;
            }
            Console.WriteLine(status);
        }

        /// <summary>
        /// Insert the ACs into the aircraft's queue with specific priority into the right place to maintain orderd Priority Queue
        /// </summary>
        /// <param name="ac">Aircraft</param>
        /// <returns></returns>
        static string Enqueue(Aircraft ac)
        {
            string statusMessage = string.Empty;
            try {
                // add an AC based on priority
                // the queue is not empty and item must be inserted in the right place
                int index = ACQueue.FindLastIndex(x => x.Priority == ac.Priority);
                if (index == ACQueue.Count - 1)
                {
                    // insert to the end of queue
                    // this will cover first and last element
                    ACQueue.Add(ac);
                }
                else if (index >= 1)
                {
                    ACQueue.Insert(index + 1, ac);
                }
                else // first AC with this Priority
                {
                    // so we add it and reorder the list
                    // better way is to find exact place in the queue then insert
                    ACQueue.Add(ac);
                    ACQueue = ACQueue.OrderBy(x => x.Priority).ThenBy(x => x.CreatedDate).ToList();
                }
                statusMessage = string.Format("AC Enqueued successfully. Number of AC in the Queue: {0}", ACQueue.Count);
            }
            catch (Exception e)
            {
                statusMessage = e.Message;
            }
            return statusMessage;
        }

        /// <summary>
        /// Dequeue the first ACs which has the top priority
        /// </summary>
        /// <returns></returns>
        static string Dequeue()
        {
            string statusMessage = string.Empty;
            try
            {
                if (ACQueue.Count == 0)
                {
                    statusMessage = "No AC exists in the queue!";
                }
                else
                {
                    // remove first item in the queue which has the most top priority
                    ACQueue.RemoveAt(0);
                    statusMessage = string.Format("AC Dequeued successfully. Number of AC in the Queue: {0}", ACQueue.Count);
                }
            }
            catch (Exception e)
            {
                statusMessage = e.Message;
            }
            return statusMessage;
        }

        /// <summary>
        /// Start the System
        /// </summary>
        /// <returns></returns>
        static string Boot()
        {
            ACQueue = new List<Aircraft>();
            return "The system has been started!";
        }

        /// <summary>
        ///  Helper method to print a list of object to the Windows Console
        /// </summary>
        /// <param name="acList"></param>
        static void PrintACList(List<Aircraft> acList)
        {
            foreach (Aircraft ac in acList)
            {
                Console.WriteLine(string.Format("{0}\t\t{1}\t\t{2}\t\t{3}", ac.ACType, ac.ACSize, ac.Priority, ac.CreatedDate));
            }
        }
    }
}
